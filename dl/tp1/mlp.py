import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader

import pytorch_lightning as pl

from collections import OrderedDict

class MLP(pl.LightningModule):
    def __init__(self, batch_size,
                    learning_rate,
                    weight_decay,
                    train_dataset,
                    train_optim,
                    loss,
                    metrics,
                    init_layers,
                    forward):
        super().__init__()
        self.bsz = batch_size
        self.lr = learning_rate
        self.wd = weight_decay
        self.train_dataset = train_dataset
        self.train_optim = train_optim
        self.loss = loss
        self.metrics = metrics
        self.forward_custom = forward
        init_layers(self)
        self.init_metrics()

    def set_test_dataloader(self, test_dataset):
        self.test_dataset = test_dataset

    def forward(self, x):
        return self.forward_custom(self, x)

    def init_metrics(self):
        self.train_loss_mean = 0
        self.test_loss_mean = 0
        self.train_metric_mean = {}
        self.test_metric_mean = {}
        self.history = {'loss':[]}
        self.test_history = {'loss':[]}
        for key, value in self.metrics.items():
            self.train_metric_mean[key] = 0
            self.test_metric_mean[key] = 0
            self.history[key] = []
            self.test_history[key] = []

    def train_dataloader(self):
        return  DataLoader(self.train_dataset, batch_size=self.bsz)

    def configure_optimizers(self):
        return self.train_optim(self.parameters(), lr=self.lr, weight_decay=self.wd)

    def tqdm_keynames(self, key, phase):
        metric_name = phase + '_' + key
        mean_metric_name = phase + '_' + key + '_mean'
        return metric_name, mean_metric_name

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.loss(y_hat, y)
        tqdm_dict = {'train_loss': loss}
        # metrics
        for key, value in self.metrics.items():
            metric = value(y, y_hat)
            metric_name, mean_metric_name = self.tqdm_keynames(key, 'train')
            tqdm_dict[metric_name] = metric
            tqdm_dict[mean_metric_name] = self.train_metric_mean[key]
        output = OrderedDict({
            'loss': loss,
            'progress_bar': tqdm_dict,
        })

        return output

    def training_epoch_end(self, outputs):
        self.train_loss_mean = 0
        self.train_metric_mean = {}
        for key, value in self.metrics.items():
            self.train_metric_mean[key] = 0

        for output in outputs:
            train_loss = output['train_loss']
            self.train_loss_mean += train_loss
            for key, value in self.metrics.items():
                metric_key, mean_metric_key = self.tqdm_keynames(key, 'train')
                train_metric = output[metric_key]
                self.train_metric_mean[key] += train_metric

        tqdm_dict = {'train_loss': train_loss}
        self.train_loss_mean /= len(outputs)
        self.history['loss'].append(self.train_loss_mean)
        for key, value in self.metrics.items():
            metric_key, mean_metric_key = self.tqdm_keynames(key, 'train')
            train_metric = output[metric_key]
            self.train_metric_mean[key] /= len(outputs)
            self.history[key].append(self.train_metric_mean[key])
            tqdm_dict[metric_key] = train_metric
            tqdm_dict[mean_metric_key] = self.train_metric_mean[key]

        result = {'progress_bar': tqdm_dict, 'loss': train_loss}
        return result

    def test_dataloader(self):
        return DataLoader(self.test_dataset)

    def test_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.loss(y_hat, y)

        tqdm_dict = {'test_loss': loss}
        # metrics
        for key, value in self.metrics.items():
            metric = value(y, y_hat)
            metric_name, mean_metric_name = self.tqdm_keynames(key, 'test')
            tqdm_dict[metric_name] = metric
            tqdm_dict[mean_metric_name] = self.test_metric_mean[key]
        output = OrderedDict({
            'loss': loss,
            'progress_bar': tqdm_dict,
        })

        return output

    def test_epoch_end(self, outputs):
        self.test_loss_mean = 0
        self.test_metric_mean = {}
        for key, value in self.metrics.items():
            self.test_metric_mean[key] = 0

        for output in outputs:
            test_loss = output['test_loss']
            self.test_loss_mean += test_loss
            for key, value in self.metrics.items():
                metric_key, mean_metric_key = self.tqdm_keynames(key, 'test')
                test_metric = output[metric_key]
                self.train_metric_mean[key] += test_metric

        tqdm_dict = {'test_loss': test_loss}
        self.test_loss_mean /= len(outputs)
        self.history['loss'].append(self.test_loss_mean)
        for key, value in self.metrics.items():
            metric_key, mean_metric_key = self.tqdm_keynames(key, 'test')
            train_metric = output[metric_key]
            self.rest_metric_mean[key] /= len(outputs)
            self.history[key].append(self.test_metric_mean[key])
            tqdm_dict[metric_key] = test_metric
            tqdm_dict[mean_metric_key] = self.test_metric_mean[key]

        result = {'progress_bar': tqdm_dict, 'loss': test_loss}
        return result
